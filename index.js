const express = require('express');
const fs = require("fs");
const bodyParser = require('body-parser');
const path = require('path');
const morgan = require('morgan');
const app = express();

const PORT = 8080;

let logStream = fs.createWriteStream(__dirname + '/logger.log',{flags: 'a'});

app.use(morgan('combined', {stream: logStream}))
try {
  app.use(bodyParser.json());
} catch {
  return res.status(500).json({message: `500 error`});
}

app.route('/api/files')
  .post(function(req, res) {  
    const dir = './files';
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    };

    if (!req.body.filename) {
      return res.status(400).json({message: `Please specify 'content' parameter`});
    }
    
    let filename = req.body.filename;
    console.log(filename)
    let content = req.body.content;
    createFile(filename, content);
    res.status(200).json({message: `File created successfully`})
    } 
  )
  
  .get(function(req, res) {
      const routersPath = path.join(__dirname, 'files');
      try {
      let files = fs.readdirSync(routersPath, (error, files) => {
        if(error) return res.status(error.statusCode || 500).json({message: `500 error`});
        return files;
      })
      res.status(200).json({
      "message": "Success",
      "files": files
    })
    } catch {
      return res.status(500).json({"message": "Server error"});
    }
  })

app.get(`/api/files/:filename`, function (req, res) {
  let arr = req.url.split('/');
  let filename = arr[arr.length - 1];
  var ext = path.extname(filename||'').split('.').pop();
  let fullName = './files/' + filename;
  let myDate = new Date().toISOString().split('.')[0] + 'Z'
  try{
    let result = fs.readFileSync(fullName, 'utf8', function(err, content) {
          if (err) return res.status(400).json({message: `No file with ${filename} filename found`});
          console.log(content);
          return content;
      })
        res.status(200).json({
        "message": "Success",
        "filename": filename,
        "content": result,
        "ext": ext,
        "uploadedDate": myDate
      })
  } catch {
    return res.status(400).json({message: `No file with ${filename} filename found`});
  }
}); 

app.listen(PORT);

function createFile(filename, content) {
  let fullName = './files/' + filename
  fs.writeFile(fullName, content, function(err) {
    if (err) return console.log(err);
    console.log(`File ${filename} saved`);
  });
}

function changeFile(filename, content) {
  let fullName = './files/' + filename
  fs.appendFile(fullName, content, function(err) {
    if (err) return console.log(err);
    console.log(`File ${filename} updated`);
  });
}

function deleteFile(filename) {
  let fullName = './files/' + filename
    fs.unlink(fullName, function(err) {
        if (err) return console.log(err);
         console.log(`File ${filename} deleted`);
    })
}
